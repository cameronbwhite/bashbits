if [ $# -ge 2 ]
then
    username=$1    
else
    read "Enter github username: " username
fi     

if [ $2 == "https" ]
then
    url_type="clone_url"
elif [ $2 == "ssh" ]
then
    url_type="ssh_url"
elif [ $2 == "git" ]
then
    url_type="git_url"
else
    url_type="clone_url"
fi

for i in $(curl https://api.github.com/users/${username}/repos | grep $url_type | sed "s/\"$url_type\": \"\([^\"]*\)\",/\1/")
do 
    git clone  $i 
done
