if [ $# -ge 1 ]
then
    username=$1    
else
    read "Enter github username: " username
fi     

curl https://api.github.com/users/$username/repos | awk -F ': ' '$1 ~ /"name"/ {print substr($2, 2, length($2)-3)}'
