if [ $# -ge 2 ]
then
    username=$1    
else
    read "Enter github username: " username
fi     

if [ $2 == "https" ]
then
    url_type="clone_url"
elif [ $2 == "ssh" ]
then
    url_type="ssh_url"
elif [ $2 == "git" ]
then
    url_type="git_url"
else
    url_type="clone_url"
fi

for i in $(curl https://api.github.com/users/$username/starred?per_page=100 | awk -F': ' '$0 ~ /$url_type/ { print substr($2,2,length($2)
do
    git clone $i
done
